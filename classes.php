<?php include("header.php"); ?>
<!-- START formerClasses.php -->
<!-- Page Content -->

   <div class="prepend-1 span-22 prepend-1 last">
    <div class="heading-major">Classes</div>
<hr>
	<strong><h3>Post-Bac - University of Washington-Bothell - September 2013 to present</h3></strong>
<hr>
<strong><u>Winter Quarter 2015</u></strong>
<li>CSS 490: Tactical Software Engineering</li>
<li>CSS 497: Computer Science and Software Engineering Capstone - Cooperative Education</li>
<hr>
<strong><u>Fall Quarter 2014</u></strong>
<li>CSS 430: Operating Systems</li>
<li>CSS 457: Multimedia and Signal Computing</li>
<li>CSS 475: Database Systems</li>
<hr>
<strong><u>Spring Quarter 2014</u></strong>
<li>CSS 350: Management Principles for Computing Professionals</li>
<li>CSS 370: Analysis and Design</li>
<li>CSS 422: Hardware and Computer Organization</li>
<hr>
<strong><u>Winter Quarter 2014</u></strong>
	<li>CSS 301: Technical Writing for Computing Professionals</li>
	<li>CSS 343: Algorithms, Data Structures, and Discrete Math II</li>
	<li>CSS 360: Software Engineering</li>
<hr>
<strong><u>Fall Quarter 2013</u></strong>
	<li>CSS 342: Algorithms, Data Structures, and Discrete Math I</li>
<hr>
	<strong><h3>Post-Bac - Bellevue College - June 2012 to December 2013</h3></strong>
<hr>
<strong><u>Fall Quarter 2013</u></strong>
	<li>MATH 130: Intro to Statistics</li>
<hr>
<strong><u>Spring Quarter 2013</u></strong>
	<li>MATH 208: Intro to Linear Algebra</li>
	<li>MATH& 254: Calculus IV</li>
	<li>PHYS 121: General Engineering Physics I</li>
<hr>
<strong><u>Winter Quarter 2013</u></strong>
	<li>MATH& 153: Calculus III</li>
<hr>
<strong><u>Fall Quarter 2012</u></strong>
	<li>MATH& 152: Calculus II</li>
<hr>
<strong><u>Summer Quarter 2012</u></strong>
	<li>MATH& 151: Calculus I</li>
<hr>
<strong><h3>Post-Bac - Lake Washington Institute of Technology - April 2012 to June 2012</h3></strong>
<hr>
<strong><u>Spring Quarter 2012</u></strong>
	<li>MATH& 142: Pre-Calculus II</li>
<hr>
<strong><h3>Special Student - University of Wisconsin-Madison - September 2011 to December 2011</h3></strong>
<hr>
<strong><u>Fall 2011</u></strong>
	<li>BIO 375: Engage Children in Science - Lead After School Science Clubs</li>
	<li>CS 367: Data Stuctures</li>
<hr>
<strong><h3>Graduate - University of Wisconsin-Madison - September 2009 to August 2011</h3></strong>
<hr>
<strong><u>Summer 2011</u></strong>
	<li>GWS 999: Independent Study - MA Thesis</li>

<hr>
<strong><u>Spring 2011</u></strong>
	<li>COMP SCI 302: Intro to Programming</li>
	<li>GWS 699: Directed Study - Feminist Theory</li>
	<li>GWS 999: Independent Study - MA Thesis</li>

<hr>
<strong><u>Fall 2010</u></strong>
	<li>ENG 341: Gender and Language</li>
	<li>GWS 679: Visual Culture, Gender, and Critical Race Theory</li>
	
<hr>	
<strong><u>Spring 2010</u></strong>
	<li>GWS 354: Women and Gender in the U.S. since 1870</li>
    <li>GWS 900: Approaches to Research in Women's Studies/Gender Studies</li>
  
<hr>
<strong><u>Fall 2009</u></strong>
	<li>MED HIST 524: The Medical History of Sex and Sexuality</li>
	<li>GWS 720: Intro to Graduate Study in Gender and Women's Studies</li>
	<li>SOC 611: Gender, Science, and Technology</li>
<hr>
<strong><h3>Undergraduate - University of Wisconsin-Madison - September 2004 to May 2009</h3></strong>
<hr>
<strong><u>Spring 2009</u></strong>
	<li>COM ARTS 613: Stardom in U.S. Film and Media</li>
	<li>DANCE 153: Asian American Movement</li>
	<li>GWS 215: Gender and Work in Rural America</li>
	<li>GWS 640: Advanced Seminar in Women's Studies</li>
<hr>
<strong><u>Fall 2008</u></strong>
	<li>COM ARTS 325: Mass Media and Human Behavior</li>
	<li>COM ARTS 355: Intro to Media Production</li>
	<li>GWS 325: Global Feminisms</li>
	<li>GWS 441: Contemporary Feminist Theories</li>
<hr>
<strong><u>Summer 2008</u></strong>
	<li>COM ARTS 451: Television Criticism</li>
<hr>
<strong><u>Spring 2008</u></strong>
	<li>COM ARTS 613: American Cinema Since 1970</li>
	<li>E ASIAN 763: Anime and Postmodernism</li>
	<li>MED HIST 531: Women and Health in American History</li>
	<li>GWS 642: Advanced Seminar in LGBT Studies</li>
<hr>
<strong><u>Fall 2007</u></strong>
	<li>COM ARTS 352: History of World Cinema</li>
	<li>PHILOS 211: Elementary Logic</li>
	<li>RELIG ST 273: Religion in History and Culture: The East</li>
	<li>GWS 421: Constructions of Gender in the Media</li>
<hr>
<strong><u>Fall 2006</u></strong>
	<li>COM ARTS 350: Introduction to Film</li>
	<li>COM ARTS 351: Introduction to Television</li>
	<li>LITTRANS 368: Modern Japanese Fiction</li>
	<li>GWS 442: Lesbian Culture</li>
<hr>
<strong><u>Spring 2006</u></strong>
	<li>COM ARTS 250: Survey of Radio, Television, and Film as Mass Media</li>
	<li>COM ARTS 260: Communication and Human Behavior</li>
	<li>RELIG ST 364: Introduction to Buddhism</li>
	<li>GWS 103: Women and Their Bodies in Health and Disease</li>
	<li>GWS 200: Intro to LGBT Studies</li>
<hr>
<strong><u>Fall 2005</u></strong>
	<li>FOOD SCI 120: Science of Food</li>
	<li>FRENCH 203: Third Semester French</li>
	<li>LITTRANS 373: Japanese Cinema</li>
	<li>GWS 102: Women, Social Institutions, and Social Change</li>
<hr>
<strong><u>Spring 2005</u></strong>
	<li>ASTRON 103: The Evolving Universe</li>
	<li>E ASIAN 253: Intro to Japanese Culture and Civilization</li>
	<li>FRENCH 102: Second Semester French</li>
	<li>PHILOS 101: Introduction to Philosophy</li>
<hr>
<strong><u>Fall 2004</u></strong>
	<li>E A STDS 222: Intro to East Asian Civilization</li>
	<li>FRENCH 101: First Semester French</li>
	<li>HIST 200: Historical Studies - The Korean War</li>
	<li>POLI SCI 103: Intro to International Relations</li>
</div>

	
	



<!-- END formerClasses.php -->
<?php include("footer.php"); ?>
 

            
 
          
