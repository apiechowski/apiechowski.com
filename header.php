<!-- START header.php -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Anna Piechowski</title>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

<meta name="google-site-verification" content="avDDeR1q1UBialJizwhEkSMfENZ6O92jcAEzG4jmCCs" />
<META name="y_key" content="bd666ee0f2ad220c">
<meta name="msvalidate.01" content="6EDE0FDD568454B3AEE37AE3C6469ADB" />

<META name="description" content="Anna Piechowski is a student at the University of Wisconsin - Madison.">
<META name="keywords" content="College, Madison, Wisconsin, Bothell, Washington University, University of Wisconsin Madison, Communication Arts, Gender, Studies, Women, Women's Studies, Gender Studies, Computer, Computer Science, Software, Software Engineering, Developer, Technical Writer">

    
    <link rel="stylesheet" href="/css/blueprint/screen.css" type="text/css" media="screen, projection">
    <link rel="stylesheet" href="/css/blueprint/print.css" type="text/css" media="print">
    <!--[if IE]>

    <link rel="stylesheet" href="/css/blueprint/ie.css" type="text/css"    media="screen, projection" />

    <![endif]-->
    <link rel="Stylesheet" type="text/css" href="/css/nav.css">
    <link rel="Stylesheet" type="text/css" href="/css/base.css">
    
</head><body>
    <div class="container">
        <div class="span-24 last">
            <ul id="nav">
                <li id="nav-home">
                    <a class="tab first" href="index.php">Home</a>
                    <div class="nav-menu-content">
                        <div class="clear pad-10">
                            <div class="nav-heading">
                                Home</div>
                            <ul>
                                <!-- <li><a href="#">#</a></li> -->
                            </ul>
                        </div>
                    </div>
                </li>
                <li id="nav-classes"><a class="tab" href="classes.php">Classes</a>
                    <div class="nav-menu-content">
                        <div class="clear pad-10">
                            <div class="nav-heading">
                                Classes</div>
                            <ul>
                                <!-- <li><a href="#">#</a></li> -->
                            </ul>
                        </div>
                    </div>
                </li>
                <li id="nav-resume"><a class="tab" href="resume.php">Resume</a>
                    <div style="display: none;" class="nav-menu-content">
                        <div class="clear pad-10">
                            <div class="nav-heading">
                                Resume</div>
                            <ul>
                                <li><a href="/uploads/piechowskiWebResume2015.pdf">Download PDF of Resume</a></li> 
                            </ul>
                        </div>
                    </div>
                </li>
				                <li id="nav-classes"><a class="tab" href="portfolio.php">Portfolio</a>
                    <div class="nav-menu-content">
                        <div class="clear pad-10">
                            <div class="nav-heading">
                                Portfolio</div>
                            <ul>
                                <!-- <li><a href="#">#</a></li> -->
                            </ul>
                        </div>
                    </div>
                </li>
				<li id="nav-contact"><a class="tab" href="contact.php">Contact Me</a>
                    <div style="display: none;" class="nav-menu-content">
                        <div class="clear pad-10">
                            <div class="nav-heading">
                                Contact Me</div>
                            <ul>
                                <!-- <li><a href="#">#</a></li> -->
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        
		<!-- Picture of Me -->
        <div class="span-24 prepend-top">
            <p align="center">
				<?php include './headerImages/RandomImagesFromDirectory.php'; ?>
            </p>
            <hr>
        </div>

<!-- END header.php -->