<?php include("header.php"); ?>
<!-- START portfolio.php -->
<!-- Page Content -->

<div class="prepend-1 span-22 prepend-1 last">
    <div class="heading-major">Portfolio</div>
<hr>

<strong><h3>Programming</h3></strong>
<hr>
<strong><u>The WebGL/three.js Experiments(2015)</u></strong></li>
	<p>Three interactive scenes that explore 3D graphics programming using the three.js library.</p>
	<ul>
		<li><a href="/threejsexperiments/HelloThreeJS.html">HelloThreeJS</a><br />
		Displays three basic shapes allows the user to perform basic transformations using keyboard input.</li>
		<li><a href="/threejsexperiments/LightsAndMaterials.html">LightsAndMaterials</a><br />
		Displays some basic shapes and allows the user to explore the effects of different lighting and materials.  Includes transparency effects and camera movement.</li>
		<li><a href="/threejsexperiments/IglooTown.html">IglooTown</a><br />
		An interactive 3D winter scene with igloos, snow, day/night transitions, and a waddling penguin.</li>
	</ul>
<hr>
<strong><h3>Technical Writing</h3></strong>
<hr>
<strong><u>Predixion Software (2014-2015)</u></strong>
	<p>A selection of work that I completed during my technical writing internship at Predixion Software, Inc.</p>
	<ul>
	<li><a href="https://predixionsoftware.com/predixiondyk/webframe.html#Did%20You%20Know.html">"Did You Know?" Email Archive</a><br />
	While at Predixion, I took over the "Did You Know?" email campaign.  I was involved in all levels of the process, including helping to generate stories and concepts; writing and editing content; and pushing articles through review and out to our customers.  This archive showcases all of the emails I contributed to and helped see through to shipment.</li>
	<li><a href="https://predixionsoftware.com/Help/webframe.html#All_Views.html">Predixion Insight Visualization Documentation</a><br />
	Although I worked in many areas of help, documenting Predixion's interactive visualizations was my primary project. With the release of Predixion 4.0, all of their visualizations transitioned from Silverlight to HTML5, altering them dramatically.  I authored new documentation for each of Predixion's 36 different visualizations.  This page provides links to the documentation for each visualization.</li>
	</ul>
<hr>
<strong><u>Scratch Lessons (2011)</u></strong>
	<p>A selection of lessons written for a middle school computer programming club.<br />
	They use example-based walkthroughs to teach students the basics of programming in Scratch.</p>
		<ul>
		<li><a href="/uploads/wrightScratchLesson2.pdf">Lesson 2: Name Game</a><br />
		<li><a href="/uploads/wrightScratchLesson3.pdf">Lesson 3: Shark Attack Game</a><br />
		<li><a href="/uploads/wrightScratchLesson4.pdf">Lesson 4: Broadcasting</a><br />
		</ul>
<hr>
<strong><u>eBay Tutorials (2010-2011)</u></strong>
	<p>A set of tutorials written to help my mother create auctions for eBay.<br />
	They cover using eBay's auction management software (TurboLister) and web-based photo hosting (imageshack).</p>
		<ul>
		<li><a href="/uploads/ebayTurbolister101.pdf">eBay's TurboLister Tutorial</a><br />
		<li><a href="/uploads/imageshackTutorial.pdf">Imageshack Tutorial</a><br />
		</ul>
<hr>

<strong><h3>Academic Writing</h3></strong>
<hr>
<strong><u>Thesis (2011)</u></strong>
	<p>Thesis written for the completion of my MA in Gender and Women's Studies.</p>
		<ul>
		<li><a href="/uploads/piechowskiMastersThesis.pdf">"Unwanted Attention": Construction of the menstrual 
			body in menstrual product advertisements in <em>Seventeen</em> magazine, 1970-2010</a><br />
		</ul>
<hr>
<strong><u>Book Review (2010)</u></strong>
	<p>A review of <em>The Modern Period: Menstruation in Twentieth-Century America</em> by Lara Freidenfelds. <br />
	Published in Volume 54, Issue 4 of the journal <em>Medical History</em> (Cambridge University Press)</p>
		<ul>
		<li><a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2948699/">Book Reviews: The Modern Period</a><br />
		</ul>
<hr>

<strong><h3>Websites, Blogs, and Multimedia Productions</h3></strong>
<hr>
<strong><u>Wedding Website (2013-2014)</u></strong>
	<p>Personal website developed for a wedding.  This was a cooperative project. <br /> 
	I was largely responsible for developing and desiging content, layout, graphics, and image display.</p>
	<ul>
	<li><a href="http://wilcoxpiechowski.com">Wilcox-Piechowski wedding website</a><br />
	</ul>
<hr>
<strong><u>Offbeat Bride Blog Guespost (2013)</u></strong>
	<p>Guestpost written for a popular bridal blog about a craft project I completed.<br />
		<ul>
		<li><a href="http://offbeatbride.com/2013/10/whovian-wedding-party-box#.UzR3J4Wna6V">
			Offbeat Bride Blog: "Will you be our Companions?: Whovian wedding party proposal boxes"</a><br />
		</ul>
<hr>
<strong><u>Documentary Short (2008)</u></strong>
	<p>Documentary short about the women's hockey club at the University of Wisconsin-Madison.<br />
	Co-shot and independently edited as part of a media production course.</p>
		<ul>
		<li><a href="http://youtu.be/0stjd8mpQa4">UW Women's Hockey Club Documentary</a><br />
		</ul>




<!-- END portfolio.php -->
<?php include("footer.php"); ?>