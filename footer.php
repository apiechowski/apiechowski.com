<!-- START FOOTER -->
            <div id="footer">
				<hr>
                <div class="prepend-8 span-8">
                    
                    <!-- Legal Stuff -->
                    <p align="center">
                        Site Updated: April 20, 2015.
                        <br>
                        Copyright &copy;2015 Anna Piechowski
                    </p>
                </div>
				<div class="span-5 last">
					<a href="http://facebook.com/anmapie"><img src="/feeds/facebook.png"/></a>
					<a href="http://linkedin.com/in/annapiechowski"><img src="/feeds/linkedin.png"/></a>
					<a href="https://plus.google.com/102143657221416908718/about/p/pub"><img src="/feeds/google-plus.png"/></a>
					<a href="http://pinterest.com/anmapie"><img src="/feeds/pinterest.png"/></a>
					<!-- <a href="http://twitter.com/"><img src="/feeds/twitter.png"/></a> -->
					<!-- <a href="http://apiechowski.com"><img src="/feeds/rss.png"/></a> -->
				</div>
            </div>
        </div>
    </div>

    <script src="/scripts/jquery-1.3.2.min.js" type="text/javascript"></script>

    <script src="/scripts/nav/nav.js" type="text/javascript"></script>

    
    

    <script type="text/javascript">    
        $(document).ready(function() {
            if (window.location.toString().indexOf("localhost") > 0) {
                $(".container").addClass("showgrid");
            }
            if (window.location.toString().indexOf("dev.apiechowski.com") > 0) {
                $(".container").addClass("showgrid");
            }
        });
    </script>

</body></html>
<!-- END FOOTER -->
