<?php include("header.php"); ?>
<!-- START resume.php -->
<!-- Page Content -->

<div class="prepend-1 span-22 append-1 last">

            

<p align="center"><font size = 5><strong>Anna Piechowski</strong></font><br/>
anna AT apiechowski dot com<br />
<a href="/uploads/piechowskiWebResume2015.pdf">Click here to download a PDF of my resume</a></p>
<hr> 

<h3><u>Skills</u></h3>
<p>
<strong>Programming Languages: </strong>C#, Java, C++, Go<br/ >
<strong>Development tools: </strong>Visual Studio, Eclipse, Unity, TFS, Git<br/ >
<strong>Office: </strong>Excel, OneNote, Outlook, Visio, Word<br/ >
</p>
<hr>

<h3><u>Experience</u></h3>
<p><strong>Software Development Intern</strong><br/>
January 2015 - present<br/>
UW-Bothell Digital Future Lab, Bothell, WA</p>
<ul>
<li>Developed features for an interactive 2D puzzle game using Unity and C#</li>
<li>Collaborated with a design team to turn abstract ideas and requirements into functional game components</li>
<li>Overhauled level progression, level loading, and score system mechanics</li>
<li>Introduced new team processes for tracking bugs and features</li>
</ul>

<p><strong>Technical Writing Intern</strong><br/>
April 2014 - April 2015<br/>
Predixion Software, Redmond, WA</p>
<ul>
<li>Authored an revitalized documentation for a major predictive analytics software product release with a focus on user education and user experience</li>
<li>Drove content creation and shipping of a technical outreach email campaign to increase user knowledge and product adoption, reaching approximately 2,000 users per week.</li>
<li>Interacted with a software development team to ship a high-quality product and promote a positive, consistent user experience.</li>
</ul>

<p><strong>Math Tutor</strong><br/>
January 2013 - January 2014<br/>
Bellevue College, Bellevue, Washington</p>
<ul>
<li>Assisted up to 25 traditional and non-traditional students daily with math queries in both a high-volume, drop-in tutoring lab setting and one-on-one sessions</li>
<li>Translated and simplified math problems for students in ways that encouraged understanding, abstraction, and retention</li>
</ul>

<p><strong>Gender and Women's Studies Teaching Assistant</strong><br />
August 2010 - May 2011<br />
University of Wisconsin-Madison</p>
<ul>
	<li>Created and implemented lectures and activities that made biological science accessible and relevant to a non-science audience</li>
	<li>Managed records, deadlines, meetings, and a course website for 80 students in 4 class sections while maintaining a graduate-level course load</li>
</ul>
<hr>
<h3><u>Projects (click titles to view)</u></h3>
<p>
<a href="/threejsexperiments.php"><strong>WebGL/three.js Experiments</strong></a><br />
<ul>
<li>Learned a new programming language (JavaScript) and how to work with a 3D graphics API</li>
<li>Implemented a variety of basic graphics elements, including a shader using GLSL, matrix transformations, dynamic lighting, and a particle system</li>
<li>Used a 3D modeling program (Clara.io) to create a unique 3D object from scratch</li>
<li>Utilized user input APIs (THREEx and dat.gui) to create interactive web applications</li>
</ul>
</p>
<p>
<a href="http://madisonscratch.azurewebsites.net"><strong>Scratch Programming Lessons</strong></a><br />
<ul>
<li>Created and adapted 8 lessons for a Scratch programming club for middle school students</li>
<li>Transformed basic coding principles into engaging, age-appropriate material</li>
<li>Led a small group of college students in teaching scratch to club members</li>
</ul>
</p>
<hr>

<h3><u>Education</u></h3>

<p><strong>Bachelor of Science, Computer Science and Software Engineering</strong><br />
University of Washington-Bothell<br />
June 2015 (expected)<br />
GPA: 3.92/4.0 </p>
	
<p><strong>Master of Arts, Gender and Women�s Studies</strong><br />
Thesis: Menstrual product advertisements in Seventeen magazine, 1970-2010<br />
University of Wisconsin-Madison<br />
August 2011<br />
GPA: 4.0/4.0</p>

<p><strong>Bachelor of Arts, Communication Arts and Women's Studies</strong><br />
 Certificates: LGBT Studies, East Asian Studies<br />
 University of Wisconsin-Madison<br />
 May 2009<br />
GPA: 3.98/4.0 </p>



<!-- END resume.php -->
<?php include("footer.php"); ?>