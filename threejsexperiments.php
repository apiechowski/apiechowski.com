<?php include("header.php"); ?>
<!-- START threejsexperiments.php -->
<!-- Page Content -->

   <div class="prepend-1 span-22 prepend-1 last">
    <div class="heading-major">The WebGL/three.js Experiments</div>
<hr>

	<p>Three interactive scenes that explore 3D graphics programming using the three.js library.</p>
	<ul>
		<li><a href="/threejsexperiments/HelloThreeJS.html">HelloThreeJS</a><br />
		Displays three basic shapes allows the user to perform basic transformations using keyboard input.</li><br />
		<li><a href="/threejsexperiments/LightsAndMaterials.html">LightsAndMaterials</a><br />
		Displays some basic shapes and allows the user to explore the effects of different lighting and materials.  Includes transparency effects and camera movement.</li><br />
		<li><a href="/threejsexperiments/IglooTown.html">IglooTown</a><br />
		An interactive 3D winter scene with igloos, snow, day/night transitions, and a waddling penguin.</li><br />
	</ul>
</div>

<!-- END threejsexperiments.php -->
<?php include("footer.php"); ?>