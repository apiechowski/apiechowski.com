<?php include("header.php"); ?>
<!-- START index.php -->
<!-- Page Content -->
<div class="prepend-1 span-22 append-1 last">

<p> I am Anna Piechowski, a current student in the University of Washington-Bothell Computer Science and Software Engineering (CSSE) program. I am interested in customer-oriented development, game development, and technical writing.  My preferred languages are C# and Java, but I also have experience with C++ and Go.  I am  learning Python and Ruby as well.</p>

<p>Currently, I am seeking a full-time position with a local software development or web services business, preferably as a developer.</p>

<p>Additionally, I am a 2011 graduate of Master of Arts program of the department of Gender and Women's Studies at the University of Wisconsin-Madison. In my time as a graduate student, I was able to have a great number of wonderful experiences, including a year of teaching a Gender and Women's studies course and having a book review published in a medical history journal.  While in graduate school, I discovered that a career in higher education was not for me.  I have always enjoyed learning about and working with new technology, so I decided to change my career path and start taking programming courses while still in graduate school. My background in academic research and teaching brings something unique to the way I understand and communicate with users and development teams.</p>

<p>Outside of my professional pursuits, I am interested in:</p>
<ul>
<li> hiking</li>
<li> ice hockey </li>
<li> bicycling </li>
<li> motorcycling </li>
<li> baseball </li>
<li> travelling </li>
<li> video games </li>
<li> photography </li>
<li> Doctor Who </li>
</ul>
<br />
</div>
<!-- END index.php -->
<?php include("footer.php"); ?>