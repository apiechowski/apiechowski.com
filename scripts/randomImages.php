<?php 
// Gives Random Image from given Links...
// Create a simple array.
   $imagesFile= file_get_contents("./scripts/randomImages.txt");

   // Place each line of $imagesFile into array
   $images = explode("\n",$imagesFile);

//Randomizes the images array, as the rand_index will choose 
//random, but in order.  This makes it seem more random.
shuffle($images);  

//Chooses three random images, and prints them
$rand_index = array_rand($images,3);
print $images[$rand_index[0]];
print $images[$rand_index[1]];
print $images[$rand_index[2]];   

?>
