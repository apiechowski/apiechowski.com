﻿(function($) {
    var animationDuration = 50;
    $("ul#nav li a.tab").hover(function(event) {
        var hoveredLink = $(this);
        hoveredLink.addClass("nav-hover");
        $(this).parent().children("div.nav-menu-content").fadeIn(animationDuration);
        $(this).parent().mouseleave(function(event) {
            hoveredLink.removeClass("nav-hover");
            $(this).children("div.nav-menu-content").fadeOut(animationDuration);
        });
    });
})(jQuery);